from collect import load_mnist
from network import NeuralNetwork
import numpy as np

if __name__ == '__main__':
    training_data, validation_data, test_data = load_mnist()
    nn = NeuralNetwork([784, 50, 50, 50, 10], 1, 16, 100)
    nn.fit(training_data, validation_data)
